# Seting up Terraform to provision on AWS environment with Jenkins, Docker, and your Flask project


## Create Terraform Configuration
Created a file named main.tf with Terraform configuration

## Initialize and Apply Terraform
Run the following commands in the same directory as main.tf file

```
terraform init
terraform apply

```

## Access Jenkins
Once Terraform completes, you can access Jenkins using the public IP address. Wait a few minutes for Jenkins to start, and then open http://18.134.155.93:8080 in your browser.

Retrieve the initial admin password by SSH-ing into the Jenkins instance:

```
sudo cat /var/lib/jenkins/secrets/initialAdminPassword
```
## Install Docker Plugin in Jenkins
In Jenkins, go to "Manage Jenkins" > "Manage Plugins."
Switch to the "Available" tab and search for "Docker."
Install the "Docker" plugin.

## Configure Docker Cloud in Jenkins
In Jenkins, go to "Manage Jenkins" > "Configure System."
Scroll down to the "Cloud" section, and click "Add a new cloud" > "Docker."
Configure the Docker Cloud details with the Docker Host URI

## Create Jenkins Pipeline
Create a Jenkins pipeline for the project 

## Run Jenkins Pipeline
Trigger the Jenkins pipeline manually or on each merge to the master branch to build, test, and deploy your Dockerized Flask application.

