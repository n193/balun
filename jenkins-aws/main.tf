provider "aws" {
    region = "eu-west-2" # Change the region as needed
}

resource "aws_instance" "balun_instance" {
    ami           = "ami-093cb9fb2d34920ad" # Specify the Jenkins AMI
    instance_type = "t2.medium" # Adjust instance type as needed

    tags = {
        Name = "balun"
    }
    user_data = <<-EOF
              #!/bin/bash
              sudo yum update -y
              sudo yum install docker -y
              sudo service docker start
              sudo usermod -aG docker ec2-user
              sudo systemctl enable docker
              sudo docker run -d -p 8080:8080 --name balun fran123pv/balun
              EOF
}

output "balun_public_ip" {
  value = aws_instance.balun_instance.public_ip
}
